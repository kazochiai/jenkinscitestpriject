//
//  main.m
//  JenkinsCITestProject
//
//  Created by Kazuhito Ochiai on 10/10/14.
//  Copyright (c) 2014 Kazuhito Ochiai. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "KZAppDelegate.h"

NSUInteger gVal;
int main(int argc, char * argv[])
{
    gVal = 2;
    NSUInteger val = 32;
    NSMutableArray *myArray = [NSMutableArray new];
    
    NSString *format = @"val = %lu, myArrayCount = %lu, g:%lu";
    void(^printVal)() = ^{ NSLog(format, val, myArray.count, gVal);};
    
    [myArray addObjectsFromArray:@[@"str0", @"str1"]];
    val = 0;
    gVal= 3;
    format = @"myArrayCount = %lu, value = %lu, g:%lu";
    
    printVal();
    
    @autoreleasepool {
        
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([KZAppDelegate class]));
    }
}
