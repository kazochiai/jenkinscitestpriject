//
//  KZAppDelegate.h
//  JenkinsCITestProject
//
//  Created by Kazuhito Ochiai on 10/10/14.
//  Copyright (c) 2014 Kazuhito Ochiai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KZAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
