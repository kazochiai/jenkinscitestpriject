//
//  KZViewController.m
//  JenkinsCITestProject
//
//  Created by Kazuhito Ochiai on 10/10/14.
//  Copyright (c) 2014 Kazuhito Ochiai. All rights reserved.
//

#import "KZViewController.h"
#include <stdlib.h>

@interface KZViewController ()

@end

@implementation KZViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
  
    
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonPressed:(id)sender {
    
    CGFloat r = arc4random_uniform(74);
    CGFloat g = arc4random_uniform(74);
    CGFloat b = arc4random_uniform(74);

    [self.view setBackgroundColor: [UIColor colorWithRed:r/255 green:g/255 blue:b/255 alpha:1.0f]];
}

@end
